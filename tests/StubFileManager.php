<?php

declare(strict_types=1);

namespace Tests;

use Arthem\GoogleDriveBackuper\FileManagerInterface;
use Google_Service_Drive_DriveFile;
use Ramsey\Uuid\Uuid;

class StubFileManager implements FileManagerInterface
{
    private $files = [];

    private $folders = [];

    /**
     * @param string[] $files
     */
    public function setFiles(array $files): void
    {
        sort($files);

        foreach ($files as $path) {
            [$folder, $name] = explode('/', $path);

            $file = new Google_Service_Drive_DriveFile();
            $id = Uuid::uuid4()->toString();
            $file->setId($id);
            $file->setName($name);
            $file->setParents([$folder]);

            if (!isset($this->files[$folder])) {
                $this->files[$folder] = [];
                $this->folders[] = $this->createFolder($folder);
            }
            $this->files[$folder][$id] = $file;
        }
    }

    private function createFolder(string $name): Google_Service_Drive_DriveFile
    {
        $file = new Google_Service_Drive_DriveFile();
        $file->setId($name);
        $file->setName($name);
        $file->setParents(['root']);

        return $file;
    }

    public function getFiles(): array
    {
        $flattenFiles = [];
        foreach ($this->files as $folder => $files) {
            /** @var Google_Service_Drive_DriveFile $file */
            foreach ($files as $file) {
                $flattenFiles[] = $folder.'/'.$file->getName();
            }
        }

        return $flattenFiles;
    }

    public function getOrCreateFolder(string $rootFolderId, string $folderName): string
    {
        return Uuid::uuid4()->toString();
    }

    public function searchBy(string $folderId, array $criteria = [], array $orderBy = []): array
    {
        if ('root' === $folderId) {
            $filteredFiles = $this->folders;
        } else {
            $filteredFiles = $this->files[$folderId];
        }

        usort($filteredFiles, function (Google_Service_Drive_DriveFile $a, Google_Service_Drive_DriveFile $b) {
            return strcmp($a->getName(), $b->getName());
        });

        return array_filter($filteredFiles, function (Google_Service_Drive_DriveFile $file) use ($criteria) {
            $matches = true;
            if (isset($criteria['name']) && $file->getName() !== $criteria['name']) {
                $matches = false;
            }

            return $matches;
        });
    }

    public function deleteFile(Google_Service_Drive_DriveFile $file): void
    {
        if (!isset($this->files[$file->getParents()[0]][$file->getId()])) {
            throw new \RuntimeException(sprintf('File %s does not exist', $file->getName()));
        }

        unset($this->files[$file->getParents()[0]][$file->getId()]);
    }

    public function renameFile(Google_Service_Drive_DriveFile $file, string $newName): void
    {
        $file->setName($newName);
    }

    public function uploadFile(string $path, Google_Service_Drive_DriveFile $file): void
    {
    }
}
