<?php

declare(strict_types=1);

namespace Tests\Action;

use Arthem\GoogleDriveBackuper\Action\Rotate;
use PHPUnit\Framework\TestCase;
use Tests\StubFileManager;

class RotateTest extends TestCase
{
    /**
     * @dataProvider getCases
     */
    public function testRotation(array $files, string $date, array $expectedFiles)
    {
        $finder = new StubFileManager();
        $finder->setFiles($files);

        $action = new Rotate($finder);

        $date = new \DateTime($date);
        $action->rotate('root', $date);

        sort($expectedFiles);
        $files = $finder->getFiles();
        sort($files);
        $diff = array_diff($files, $expectedFiles);

        $this->assertEquals([], $diff);
        $this->assertEquals($expectedFiles, $files);

        foreach ($expectedFiles as $expectedFile) {
            $file = array_shift($files);
            $this->assertSame($expectedFile, $file);
        }
    }

    public function getCases(): array
    {
        $files = function (int $year, int $month, array $days, array $hours): array {
            $files = [];
            foreach ($days as $day) {
                foreach ($hours as $hour) {
                    $date = new \DateTime();
                    $date->setDate($year, $month, $day);
                    $date->setTime($hour, 0, 0);
                    $files[] = sprintf('%s/cat-app-prod-%s.sql.gz', $date->format('Y-m'), $date->format('Ymd-Hi'));
                }
            }

            return $files;
        };

        $renamedFiles = function (int $year, int $month, array $days): array {
            $files = [];
            foreach ($days as $day) {
                $date = new \DateTime();
                $date->setDate($year, $month, $day);
                $files[] = sprintf('%s/cat-app-prod-%s.sql.gz', $date->format('Y-m'), $date->format('Ymd'));
            }

            return $files;
        };

        return [
            [
                $files(2017, 12, [5], [1, 2]),
                '2017-12-31',
                $renamedFiles(2017, 12, [5]),
            ],
            [
                $files(2017, 12, range(10, 31), range(0, 23)),
                '2017-12-31',
                array_merge(
                    $renamedFiles(2017, 12, range(10, 16)),
                    $files(2017, 12, range(17, 31), range(0, 23))
                ),
            ],
            [
                array_merge(
                    $files(2017, 11, range(1, 30), range(0, 23)),
                    $files(2017, 12, range(1, 31), range(0, 23))
                ),
                '2017-12-31',
                array_merge(
                    $renamedFiles(2017, 11, [1, 15]),
                    $renamedFiles(2017, 12, range(1, 16)),
                    $files(2017, 12, range(17, 31), range(0, 23))
                ),
            ],
        ];
    }
}
