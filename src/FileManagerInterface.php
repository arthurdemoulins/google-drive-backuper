<?php

declare(strict_types=1);

namespace Arthem\GoogleDriveBackuper;

use Google_Service_Drive_DriveFile;

interface FileManagerInterface
{
    const TYPE_FOLDER = 'application/vnd.google-apps.folder';

    public function getOrCreateFolder(string $rootFolderId, string $folderName): string;

    /**
     * @return Google_Service_Drive_DriveFile[]
     */
    public function searchBy(string $folderId, array $criteria = [], array $orderBy = []): array;

    public function deleteFile(Google_Service_Drive_DriveFile $file): void;

    public function renameFile(Google_Service_Drive_DriveFile $file, string $newName): void;

    public function uploadFile(string $path, Google_Service_Drive_DriveFile $file): void;
}
