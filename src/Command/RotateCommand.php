<?php

declare(strict_types=1);

namespace Arthem\GoogleDriveBackuper\Command;

use Arthem\GoogleDriveBackuper\Action\Rotate;
use Arthem\GoogleDriveBackuper\AppContainer;
use Arthem\GoogleDriveBackuper\OutputLogger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RotateCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('rotate')
            ->setDefinition([
                new InputArgument('folder-id', InputArgument::REQUIRED, 'The root folder ID'),
            ])
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $date = new \DateTime();
        $folderId = $input->getArgument('folder-id');

        $container = new AppContainer();

        $logger = new OutputLogger($output);
        $action = new Rotate($container->getFileManagerService());
        $action->setLogger($logger);

        $action->rotate($folderId, $date);
    }
}
