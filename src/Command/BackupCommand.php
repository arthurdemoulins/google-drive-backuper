<?php

declare(strict_types=1);

namespace Arthem\GoogleDriveBackuper\Command;

use Arthem\GoogleDriveBackuper\Action\Backup;
use Arthem\GoogleDriveBackuper\AppContainer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BackupCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('backup')
            ->setDefinition([
                new InputArgument('path', InputArgument::REQUIRED, 'Path to the file to upload'),
                new InputArgument('folder-name', InputArgument::REQUIRED, 'The folder where to save the file'),
                new InputArgument('folder-id', InputArgument::REQUIRED, 'The Drive root folder ID'),
            ])
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $input->getArgument('path');
        $folderName = $input->getArgument('folder-name');
        $rootFolderId = $input->getArgument('folder-id');

        $container = new AppContainer();

        $action = new Backup($container->getFileManagerService());
        $action->backup($path, $rootFolderId, $folderName);
    }
}
