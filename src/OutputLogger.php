<?php

declare(strict_types=1);

namespace Arthem\GoogleDriveBackuper;

use Psr\Log\AbstractLogger;
use Symfony\Component\Console\Output\OutputInterface;

class OutputLogger extends AbstractLogger
{
    /**
     * @var OutputInterface
     */
    private $output;

    public function __construct(OutputInterface $output)
    {
        $this->output = $output;
    }

    public function log($level, $message, array $context = [])
    {
        $this->output->writeln($message);
    }
}
