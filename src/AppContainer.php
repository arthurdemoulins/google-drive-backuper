<?php

declare(strict_types=1);

namespace Arthem\GoogleDriveBackuper;

use Google_Client;
use Google_Service_Drive;

class AppContainer
{
    /**
     * @var Google_Service_Drive
     */
    private $drive;

    /**
     * @var FileManager
     */
    private $fileManager;

    public function getDriveService(): Google_Service_Drive
    {
        if (null !== $this->drive) {
            return $this->drive;
        }

        $client = new Google_Client();
        $client->useApplicationDefaultCredentials();
        $client->addScope(Google_Service_Drive::DRIVE);

        $this->drive = new Google_Service_Drive($client);

        return $this->drive;
    }

    public function getFileManagerService(): FileManager
    {
        if (null !== $this->fileManager) {
            return $this->fileManager;
        }

        $this->fileManager = new FileManager($this->getDriveService());

        return $this->fileManager;
    }
}
