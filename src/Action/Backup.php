<?php

declare(strict_types=1);

namespace Arthem\GoogleDriveBackuper\Action;

use Arthem\GoogleDriveBackuper\FileManagerInterface;
use Google_Service_Drive_DriveFile;

class Backup
{
    /**
     * @var FileManagerInterface
     */
    private $fileManager;

    public function __construct(FileManagerInterface $fileManager)
    {
        $this->fileManager = $fileManager;
    }

    public function backup(string $path, string $rootFolderId, string $folderName): void
    {
        $folderId = $this->fileManager->getOrCreateFolder($rootFolderId, $folderName);

        $file = new Google_Service_Drive_DriveFile([
            'name' => basename($path),
            'parents' => [$folderId],
            'originalFilename' => basename($path),
        ]);

        $this->fileManager->uploadFile($path, $file);
    }
}
