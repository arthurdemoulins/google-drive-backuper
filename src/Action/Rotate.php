<?php

declare(strict_types=1);

namespace Arthem\GoogleDriveBackuper\Action;

use Arthem\GoogleDriveBackuper\FileManagerInterface;
use Google_Service_Drive_DriveFile;
use Psr\Log\LoggerInterface;

class Rotate
{
    /**
     * @var FileManagerInterface
     */
    private $fileManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(FileManagerInterface $fileManager)
    {
        $this->fileManager = $fileManager;
    }

    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    public function rotate(string $folderId, \DateTime $date): void
    {
        $date->setTime(0, 0, 0);
        $limitDate = clone $date;
        $limitDate->sub(new \DateInterval('P14D'));
        $hardLimit = clone $date;
        $hardLimit->sub(new \DateInterval('P1M'));

        $this->debug(sprintf('<info>Rotating backups for %s...</info>', $folderId));

        $folders = $this->fileManager->searchBy($folderId, [
            'type' => FileManagerInterface::TYPE_FOLDER,
        ]);

        $map = [];
        foreach ($folders as $folder) {
            $this->debug(sprintf('-- <comment>%s</comment>', $folder->getName()));
            $files = $this->fileManager->searchBy($folder->getId(), [], ['createdTime' => 'asc']);

            foreach ($files as $file) {
                if (0 === preg_match('#^(?P<cat>[a-z]+)\-(?P<app>[a-z]+)\-(?P<env>dev|(?:pre)?prod)(?P<exclusion>\-with-exclusions)?\-(?P<date>\d{8}(?:\-\d{4})?)\.(?P<extension>tar|sql)\.gz$#', $file->name, $regs)) {
                    trigger_error(sprintf('File "%s" does not match backup pattern', $file->name), E_USER_WARNING);
                    continue;
                }

                $key = implode('|', [$regs['cat'], $regs['app'], $regs['env'], (empty($regs['exclusion']) ? '1' : '0'), $regs['extension']]);
                if (!isset($map[$key])) {
                    $map[$key] = [];
                }

                if (1 === preg_match('#^(\d{4})(\d{2})(\d{2})\-(\d{2})(\d{2})$#', $regs['date'], $dateRegs)) {
                } elseif (1 === preg_match('#^(\d{4})(\d{2})(\d{2})$#', $regs['date'], $dateRegs)) {
                    $dateRegs[4] = '00';
                    $dateRegs[5] = '00';
                } else {
                    throw new \RuntimeException(sprintf('Invalid date "%s"', $regs['date']));
                }

                [, $year, $month, $day, $hour, $minute] = $dateRegs;
                $fileDate = new \DateTime(sprintf('%s-%s-%s %s:%s:00',
                    $year, $month, $day, $hour, $minute
                ));

                $dateKey = $fileDate->format('Y-m-d');
                if (!isset($map[$key][$dateKey])) {
                    $map[$key][$dateKey] = [];
                }
                $map[$key][$dateKey][] = $file;
            }
        }

        foreach (range(1, 2) as $shot) {
            $this->fixMap($map, $limitDate, $hardLimit);
        }
    }

    private function fixMap(array &$map, \DateTime $limitDate, \DateTime $hardLimit): void
    {
        foreach ($map as $app => $dates) {
            [$cat, $appName, $env, $isFull, $extension] = explode('|', $app);
            $isFull = (bool) (int) $isFull;

            foreach ($dates as $date => $files) {
                $d = new \DateTime($date);
                $d->setTime(0, 0, 0);
                if ($d >= $limitDate) {
                    continue;
                }

                if (count($files) > 0) {
                    /** @var Google_Service_Drive_DriveFile[] $files */
                    $renamed = false;
                    foreach ($files as $fileKey => $file) {
                        if ($d < $hardLimit) {
                            if (!in_array((int) $d->format('j'), [1, 15], true)) {
                                $this->fileManager->deleteFile($file);
                                unset($map[$app][$date][$fileKey]);
                                continue;
                            }
                        }

                        if (!$renamed) {
                            $newName = sprintf('%s-%s-%s%s-%s.%s.gz',
                                $cat,
                                $appName,
                                $env,
                                $isFull ? '' : '-with-exclusions',
                                $d->format('Ymd'),
                                $extension
                            );

                            if ($file->getName() === $newName) {
                                continue;
                            }

                            $this->debug(sprintf('RENAME <info>%s</info> to <info>%s</info>', $file->getName(), $newName));
                            $this->fileManager->renameFile($file, $newName);
                            $renamed = true;
                        } else {
                            $this->debug(sprintf('DELETE <info>%s</info>', $file->getName()));
                            $this->fileManager->deleteFile($file);
                            unset($map[$app][$date][$fileKey]);
                        }
                    }
                }
            }
        }
    }

    private function debug(string $message): void
    {
        if (null === $this->logger) {
            return;
        }

        $this->logger->debug($message);
    }
}
