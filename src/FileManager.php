<?php

declare(strict_types=1);

namespace Arthem\GoogleDriveBackuper;

use Google_Service_Drive;
use Google_Service_Drive_DriveFile;

class FileManager implements FileManagerInterface
{
    /**
     * @var Google_Service_Drive
     */
    private $drive;

    public function __construct(Google_Service_Drive $drive)
    {
        $this->drive = $drive;
    }

    public function getOrCreateFolder(string $rootFolderId, string $folderName): string
    {
        $files = $this->searchBy($rootFolderId, [
            'name' => $folderName,
            'mimeType' => self::TYPE_FOLDER,
        ]);
        foreach ($files as $file) {
            if ($file->name === $folderName) {
                return $file->id;
            }
        }

        $fileMetadata = new Google_Service_Drive_DriveFile([
            'name' => $folderName,
            'mimeType' => self::TYPE_FOLDER,
            'parents' => [$rootFolderId],
        ]);

        $file = $this->drive->files->create($fileMetadata, [
            'fields' => 'id',
        ]);

        return $file->id;
    }

    /**
     * @return Google_Service_Drive_DriveFile[]
     */
    public function searchBy(string $folderId, array $criteria = [], array $orderBy = []): array
    {
        $criteria = array_merge([
            'parent' => $folderId,
            'trashed' => false,
        ], $criteria);

        $order = null;
        if (!empty($orderBy)) {
            $order = [];
            foreach ($orderBy as $field => $way) {
                $order[] = $field.' '.$way;
            }
            $order = implode(', ', $order);
        }

        $query = [];
        foreach ($criteria as $key => $value) {
            switch ($key) {
                case 'name':
                case 'mimeType':
                    $query[] = sprintf('%s = \'%s\'', $key, $this->escapeName($value));
                    break;
                case 'parent':
                    $query[] = sprintf('\'%s\' in parents', $this->escapeName($value));
                    break;
                case 'trashed':
                    $query[] = sprintf('%s = %s', $key, $value ? 'true' : 'false');
                    break;
            }
        }

        $query = implode(' and ', $query);

        $optParams = [
            'q' => $query,
            'fields' => 'nextPageToken, files(id, name)',
        ];
        if ($order) {
            $optParams['orderBy'] = $order;
        }

        $files = [];
        $pageToken = null;
        do {
            $optParams['pageToken'] = $pageToken;

            $response = $this->drive->files->listFiles($optParams);

            foreach ($response->files as $file) {
                $files[] = $file;
            }

            $pageToken = $response->pageToken;
        } while (null != $pageToken);

        return $files;
    }

    public function uploadFile(string $path, Google_Service_Drive_DriveFile $file): void
    {
        $this->drive->files->create($file, [
            'data' => file_get_contents($path),
            'mimeType' => 'application/octet-stream',
            'uploadType' => 'media',
        ]);
    }

    public function deleteFile(Google_Service_Drive_DriveFile $file): void
    {
        $this->drive->files->delete($file->id);
    }

    public function renameFile(Google_Service_Drive_DriveFile $file, string $newName): void
    {
        $file->setName($newName);

        $newFile = new Google_Service_Drive_DriveFile();
        $newFile->setName($newName);

        $this->drive->files->update($file->id, $newFile);
    }

    private function escapeName(string $name): string
    {
        return addcslashes($name, '\'');
    }
}
